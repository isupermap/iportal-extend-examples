import Vue from 'vue'
import App from './App.vue'
import antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css';
// import 'element-ui/lib/theme-chalk/index.css' //导入样式

// import ElementUI from 'element-ui'

// Vue.use(ElementUI)
Vue.use(antd)

Vue.config.productionTip = false;
new Vue({
  el: '#app',
  render: h => h(App)
})
