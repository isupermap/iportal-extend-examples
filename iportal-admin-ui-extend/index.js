import ant1 from './src/components/ant-design-vue-demo'
import ele1 from './src/components/elementDemo'
import 'default-passive-events'
// import 'element-ui/lib/theme-chalk/index.css' //导入样式

// import ElementUI from 'element-ui'

export function GenerateRoutes(UserAddVue, pageView, routeView) {
  const extendRoute = [//路由
    {
      path: '/extend',
      name: 'UserElement',
      component: pageView,
      meta: { title: '用户扩展组件演示', keepAlive: true},
      children: [
        // { //导入element-ui会与产生冲突，这里演示仅表示可以导入其他第三方库
        //   path: '/extend/element-ui',
        //   name: 'UserElement',
        //   component: ele1,
        //   meta: { title: 'element-ui组件演示', keepAlive: true}
        // },
        {
          path: '/extend/ant-design-vue',
          name: 'UserAntDesign',
          component: ant1,
          meta: { title: 'ant-design-vue组件演示', keepAlive: true}
        },
        {
          path: 'http://support.supermap.com.cn:8092',
          name: 'supermap',
          meta: {
            title: '新窗口打开演示',
            target: '_blank' // 打开到新窗口
          }
        }
      ]
    },
    {
      path: '/extend/iframe',
      name: 'UserIframe',
      component: UserAddVue,
      meta: { title: 'iframe嵌入页面', keepAlive: true, src: 'http://support.supermap.com.cn:8092' ,permission: ['portal:admin:appsConfig']}//权限路由
    }
    ];
  return extendRoute;
}
export function addToVue(_Vue) {
  // _Vue.use(ElementUI) //绑定ElementUI
}
