//Copyright 2000-2019 SuperMap Software Co.Ltd. All rights reserved.
let portalRoot = window.location.origin + window.iportal.contextPath;
const HomePageUtil = {
    portalRoot:portalRoot,
    // portalRoot:"http://rdc.ispeco.com/",
    webUiRoot:portalRoot+"/web-ui/",
    getPortalRoot(){
        let rootUrl = "";
        if(window.location.href.indexOf('/resource') > 0){
            var resourceIndex = window.location.href.indexOf('/resource');
            rootUrl = window.location.href.slice(0,resourceIndex);
        } 
        return rootUrl;
        // return 'http://rdc.ispeco.com/';
    },
    /**
     * 解析JSON对象中的{portalRoot}或者{webUiRoot};
     * @param {*} jsonObj JSON对象
     * @param {*} str 规定子字符串或要替换的模式的 RegExp 对象。
     * @param {*} toStr 一个字符串值。规定了替换文本。
     */
    replaceAll(jsonObj,str,toStr) {
        let jsonStr = JSON.stringify(jsonObj);
        let re = new RegExp(str,"g")
        jsonStr = jsonStr.replace(re,toStr); 
        return JSON.parse(jsonStr);
    },
    /**
     * 根据资源类型和资源排序类型获取请求资源集合的url
     * @param {*} resourceType 资源类型
     * @param {*} resourceSortType 资源排序类型
     */
    getResourceUrl(resourceType,resourceSortType){
        let url = portalRoot;
        let orderBy = "VISITCOUNT";
        let orderType = "DESC";
        let pageSize = 10;
        if(resourceType === "datas"){
            orderBy = "DOWNLOADCOUNT";
        }
        if (resourceType === 'notebooks') {
            orderBy = 'HEATLEVEL'
        }
        // 默认hot为最热门资源  另一种为last最新资源
        if(resourceSortType === "last"){
            orderBy = "UPDATETIME";
            if(resourceType === "datas"){
                orderBy = "LASTMODIFIEDTIME";
            }
        }
        let param = `orderBy=${orderBy}&orderType=${orderType}&currentPage=1&pageSize=${pageSize}`;
        if(resourceType === "maps"){
            let orderByObjStr = encodeURI(JSON.stringify([{orderField: orderBy,orderType: orderType}]));
            param = `orderBy=${orderByObjStr}&currentPage=1&pageSize=${pageSize}`;
        }
        if (resourceType === "notebooks") {
            param = `resourceType=NOTEBOOK&pageSize=${pageSize}&currentPage=1&orderBy=${orderBy}&orderType=DESC&searchType=SEARCH`;
            url = `${url}/gateway/catalog/search.json?${param}`;
        } else {
            url = `${url}/web/${resourceType}.json?${param}&t=${new Date().getTime()}`;
        }
        return url;
    },
    /**
    * 获取时间戳
    */
    getTimeStamp() {
        return `t=${(new Date()).getTime()}`;
    }
}
export default HomePageUtil;
