import Axios from "axios";
import HomePageUtil from "./HomePageUtil.js";
const RequestUtil = {
    /**
     * 获取iportal系统配置信息
     */
    getSystemConfig() {
        let url = HomePageUtil.portalRoot+`/web/config/system.json?${HomePageUtil.getTimeStamp()}`;
        return Axios.get(url);
    },
    /**
     * 获取全站UI配置信息
     */
    getCommonConfig() {
        let url = HomePageUtil.portalRoot+`/web/uiconfig/commonsetting.json?${HomePageUtil.getTimeStamp()}&language=zh`;
        return Axios.get(url);
    },
    /**
     * 获取新闻组件列表信息
     * @param {*} param 过滤新闻的参数对象
     */
    getNewsList(param) {
        let orderBy = param.orderBy ? param.orderBy : "UPDATETIME";
        let url = HomePageUtil.portalRoot + `/web/news.json?${HomePageUtil.getTimeStamp()}&published=true&orderBy=${orderBy}&orderType=DESC`;
        return Axios.get(url);
    },
    /**
     * 获取用户信息
     */
    getUserState() {
        let url = HomePageUtil.portalRoot + `/web/config/userprofile.json?${HomePageUtil.getTimeStamp()}`;
        return Axios.get(url);
    }
}
export default RequestUtil;