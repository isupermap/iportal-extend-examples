import Vue from 'vue'
import App from './App.vue'
import router from './router'
import iportalComponents from '@ispeco/iportal-components'
import '@ispeco/iportal-components/lib/iportal-components.css'
import AntD from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import VueiClient from '@supermapgis/vue-iclient-mapboxgl'

Vue.use(VueiClient);
Vue.use(AntD);
Vue.config.productionTip = false
Vue.use(iportalComponents)
router.beforeEach((to, from, next) => {
  to.matched.length === 0 ? next("/home") : next();
})
new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
