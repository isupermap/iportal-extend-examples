import Vue from 'vue'
import VueRouter from 'vue-router'

const DefHomePage = () => import(/* webpackChunkName: "DefHomePage" */  './DefHomePage');
const DefHomePage1 = () => import(/* webpackChunkName: "DefHomePage1" */  './DefHomePage1');
const HomePage = () => import(/* webpackChunkName: "HomePage" */  './HomePage');

Vue.use(VueRouter);
const router =  new VueRouter({
  mode: 'hash',
  routes: [
    {
        path: '/defhome',
        name: 'defHome',
        component: DefHomePage,
        meta: {
            title: '首页'
        }
    },
    {
        path: '/defhome1',
        name: 'defhome1',
        component: DefHomePage1,
        meta: {
            title: '首页'
        }
    },
    {
        path: '/home',
        name: 'home',
        component: HomePage,
        meta: {
            title: '首页'
        }
    },
  ]
})
export default router;