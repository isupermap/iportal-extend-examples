const path = require('path');
module.exports = {
    outputDir:"home-page",
    publicPath: './',
    // IE兼容
    transpileDependencies: [
        '@supermap/vue-iclient-mapboxgl/src',
        '@mapbox/mapbox-gl-draw',
        'resize-detector',
        'colorcolor',
        '@supermap/vue-iclient-mapboxgl/static/libs/echarts-layer',
        '@supermap/vue-iclient-mapboxgl/static/libs/json-sql'
    ],
    configureWebpack: (config) => {
        // 按需加载Vue-client 的ts所需 后面组件化后 不需要此处代码
        let newConfig = {
            resolve: {
                extensions: ['.mjs', '.js', '.vue', '.json', '.wasm', '.ts']
            },
            module: {
                rules: [
                    {
                        test: /\.(ts|tsx)$/,
                        include: [path.resolve(__dirname, 'src'), path.resolve(__dirname, 'node_modules/@supermap/vue-iclient-mapboxgl')],
                        use: [
                            {
                                loader: 'babel-loader'
                            },
                            {
                                loader: 'ts-loader',
                                options: {
                                appendTsSuffixTo: [/\.vue$/]
                                }
                            }
                        ]
                    }
                ]
            }
        };
        if (process.env.NODE_ENV === 'production') {
            // 将第三方依赖包打包成多个单独的js文件，减小只有一个js的大小
            let optimization = {
                splitChunks: {
                    chunks: 'async',
                    minSize: 30000,
                    minChunks: 1,
                    maxAsyncRequests: 5,
                    maxInitialRequests: 8,
                    name: false,
                    cacheGroups: {
                        vendor: {
                            name: 'vendor',
                            filename: './js/[name].[hash:8].js',
                            chunks: 'initial',
                            priority: -10,
                            reuseExistingChunk: true,
                            test: /[\\/]node_modules[\\/]/
                        },
                        'vue-iclient-mapboxgl': {
                            name: 'vue-iclient-mapboxgl',
                            filename: './libs/[name].js',
                            chunks: 'initial',
                            priority: 5,
                            test: /[\\/]node_modules[\\/]@supermap[\\/](vue-iclient-mapboxgl)[\\/]/
                        },
                        'iportal-components': {
                            name: 'iportal-components',
                            filename: './libs/[name].js',
                            chunks: 'initial',
                            priority: 4,
                            test: /[\\/]node_modules[\\/]@ispeco[\\/](iportal-components)[\\/]/
                        }
                    }
                }
            };
            Object.assign(config, {
                optimization
            });
            return {
                ...newConfig
            }
        }else {
            return {
                ...newConfig
            };
        }
    },
    chainWebpack: config => {
      if (process.env.NODE_ENV === 'production') {
        // 解决打包时less与css的calc方法冲突问题
        config.plugin('optimize-css').tap(([{ cssnanoOptions, ...args }]) => {
          const preset = cssnanoOptions.preset || [];
          preset.forEach(item => {
            if (typeof item === 'object') {
              item.calc = false;
            }
          });
          cssnanoOptions.preset = preset;
          return [{ cssnanoOptions, ...args }];
        });
      }
    }
}